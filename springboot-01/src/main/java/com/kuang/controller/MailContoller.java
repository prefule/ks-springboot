package com.kuang.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kuang.Service.MailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@Api(tags = "发送邮件")
@ApiSupport(author = "xxx.com", order = 1)
@RestController
@RequestMapping("/mails")
public class MailContoller {
    @Autowired
    MailService mailService;

    @ApiOperation("一个简单的邮件")
    @GetMapping("/sendMail")
    public void sendMail() {
        mailService.senMail();
    }

    @ApiOperation("一个复杂的邮件")
    @GetMapping("/sendMail2")
    public void sendMaile2() throws MessagingException {
        mailService.sendMail2();
    }
}
