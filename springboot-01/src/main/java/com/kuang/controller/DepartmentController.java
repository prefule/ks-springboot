package com.kuang.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kuang.mapper.DepartmentMapper;
import com.kuang.pojo.Department;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Api(tags = "部门")
@ApiSupport(author = "xxx.com", order = 3)
@RestController
@RequestMapping("/departments")
public class DepartmentController {
    @Autowired
    DepartmentMapper departmentMapper;

    @ApiOperation("查询全部部门")
    @GetMapping("/getDepartments")
    public List<Department> getDepartments() {
        return departmentMapper.getDepartments();
    }

    @ApiOperation("根据id查询全部部门")
    @GetMapping("/getDepartment/{id}")
    public Department getDepartment(@PathVariable("id") Integer id) {
        return departmentMapper.getDepartment(id);
    }

    @ApiOperation("添加部门")
    @GetMapping("/save")
    public int save() {
        List<String> departmentNames = Arrays.asList("教学部", "市场部", "教研部", "运营部", "后勤部");
        departmentNames.forEach(departmentName -> {
            Department department = new Department();
            department.setDepartmentName(departmentName);
            departmentMapper.save(department);
        });
        return 1;
    }
}
