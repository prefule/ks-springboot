package com.kuang.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kuang.Service.AsyncService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "异步操作")
@ApiSupport(author = "xxx.com", order = 5)
@RestController
@RequestMapping("async")
public class AsyncController {

    @Autowired
    AsyncService asyncService;

    @ApiOperation("hello")
    @GetMapping("/hello")
    public String hello() {
        asyncService.hello();
        return "success";
    }
}