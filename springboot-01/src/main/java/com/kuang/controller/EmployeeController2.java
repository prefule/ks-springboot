package com.kuang.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kuang.mapper.EmployeeMapper;
import com.kuang.pojo.Employee;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api(tags = "员工")
@ApiSupport(author = "xxx.com", order = 4)
@RestController
@RequestMapping("/employee2")
public class EmployeeController2 {

    @Autowired
    EmployeeMapper employeeMapper;

    // 获取所有员工信息
    @ApiOperation("获取所有员工信息")
    @GetMapping("/getEmployees")
    public List<Employee> getEmployees() {
        return employeeMapper.getEmployees();
    }

    @ApiOperation("添加员工")
    @GetMapping("/save")
    public int save() {
        Employee employee = new Employee();
        employee.setLastName("kuangshen");
        employee.setEmail("qinjiang@qq.com");
        employee.setGender(1);
        employee.setDepartment(1);
        employee.setBirth(new Date());
        return employeeMapper.save(employee);
    }

    @ApiOperation("通过id获得员工信息")
    @GetMapping("/get/{id}")
    public Employee get(@PathVariable("id") Integer id) {
        return employeeMapper.get(id);
    }

    @ApiOperation("通过id删除员工")
    @GetMapping("/delete/{id}")
    public int delete(@PathVariable("id") Integer id) {
        return employeeMapper.delete(id);
    }

}