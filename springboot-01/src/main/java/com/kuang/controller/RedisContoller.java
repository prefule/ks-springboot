package com.kuang.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.kuang.Service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "集成redis")
@ApiSupport(author = "xxx.com", order = 2)
@RestController
@RequestMapping("/redis")
public class RedisContoller {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedisService redisService;

    @ApiOperation("简单获取key")
    @GetMapping("/getKey")
    public String get() {
        return redisService.getKey();
    }

    @ApiOperation("序列号实体类")
    @GetMapping("/serializeObject")
    public String serializeObject() throws JsonProcessingException {
        return redisService.serializeObject();
    }

    @ApiOperation("自定义redisTemplate序列号实体类")
    @GetMapping("/serializeObject2")
    public String serializeObject2() throws JsonProcessingException {
        return redisService.serializeObject2();
    }

    @ApiOperation("队列操作")
    @GetMapping("/list")
    public void list() {
        redisService.list();
    }

    @ApiOperation("集合操作")
    @GetMapping("/set")
    public void set() {
        redisService.set();
    }
}
