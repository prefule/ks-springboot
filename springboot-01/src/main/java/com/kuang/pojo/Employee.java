package com.kuang.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@ApiModel("员工实体类")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Integer id;

    @ApiModelProperty("姓名")
    public String lastName;

    @ApiModelProperty("邮件")
    private String email;

    @ApiModelProperty("性别;1 male, 0 female")
    private Integer gender;

    @ApiModelProperty("部门id")
    private Integer department;

    private Department eDepartment; // 冗余设计

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birth;

    public Employee(Integer id, String lastName, String email, Integer gender, Department department) {
        this.id = id;
        this.lastName = lastName;
        this.email = email;
        this.gender = gender;
        this.eDepartment = department;
        this.birth = new Date();
    }
}
