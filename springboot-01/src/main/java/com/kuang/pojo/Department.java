package com.kuang.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel("部门实体类")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    private Integer id;

    private String departmentName;
}
