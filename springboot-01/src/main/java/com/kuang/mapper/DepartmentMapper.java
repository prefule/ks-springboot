package com.kuang.mapper;

import com.kuang.pojo.Department;
import com.kuang.pojo.Employee;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentMapper {

    // 获取所有部门信息
    List<Department> getDepartments();

    // 通过id获得部门
    Department getDepartment(Integer id);

    // 新增一个员工
    int save(Department department);
}