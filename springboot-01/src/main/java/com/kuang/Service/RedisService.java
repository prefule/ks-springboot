package com.kuang.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuang.pojo.Department;
import com.kuang.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Slf4j
@Service
public class RedisService {

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate redisTemplate;

    /**
     * @throws
     * @title getKey
     * @description
     * @updateTime 2023/10/22 23:28
     */
    public String getKey() {
        // redisTempTate 操作不同的数据类型，api和我们的指令是一样的
        // opsForVaTue  操作字符串 类似string
        // opsForList   操作List 类似List
        // opsForSet
        // opsForHash
        // opsForZset
        // opsForGeo
        // opsForHyperLogLog
        // 除了进本的操作，我们常用的方法都可以直接通过redisTemplate操作，比如事务，和基本的CRUD
        // 获取redis的连接对象
        //RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        //connection.flushDb();
        //connection.flushAll();

        redisTemplate.opsForValue().set("mykey", "关注狂神说公众号");
        return (String) redisTemplate.opsForValue().get("mykey");
    }

    /**
     * @throws
     * @title serializeObject
     * @description 序列号实体类
     * @updateTime 2023/10/23 8:58
     */
    public String serializeObject() throws JsonProcessingException {
        // 真实开发一般都试用json来传递
        Department department = new Department(1, "教研部");
        String jsonDepartment = new ObjectMapper().writeValueAsString(department);
        redisTemplate.opsForValue().set("department", jsonDepartment);

        return (String) redisTemplate.opsForValue().get("department");
    }

    /**
     * @throws
     * @title serializeObject
     * @description 自定义redisTemplate序列号实体类
     * @updateTime 2023/10/23 8:58
     */
    public String serializeObject2() throws JsonProcessingException {
        // 真实开发一般都试用json来传递
        Department department = new Department(2, "教研部2");
        String jsonDepartment = new ObjectMapper().writeValueAsString(department);
        redisTemplate.opsForValue().set("department", jsonDepartment);

        return (String) redisTemplate.opsForValue().get("department");
    }


    @Autowired
    private RedisUtil redisUtil;

    /**
     * @title
     * @description 队列操作
     * @updateTime 2023/10/23 9:42
     * @throws
     */
    public void list() {
        String lKey = "list";
        redisUtil.lSet(lKey, Arrays.asList(1, 2, 3, 4, 5, 6));

        log.info("size:{}", redisUtil.lGetListSize(lKey));
        log.info("list:{}", redisUtil.lGet(lKey, 0, -1));

        log.info("pop:{}", redisUtil.lPop(lKey));
        log.info("size:{}", redisUtil.lGetListSize(lKey));
        log.info("pop:{}", redisUtil.lPop(lKey, 3));

        redisUtil.lSet(lKey, 10010);
        log.info("list:{}", redisUtil.lGet(lKey, 0, -1));
    }

    /**
     * @title set
     * @description 集合操作
     * @author prefule 
     * @updateTime 2023/10/23 10:01 
     * @throws 
     */
    public void set() {
        String sKey = "set";
        redisUtil.sSet(sKey, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);

        log.info("members:{}", redisUtil.sGet(sKey));
        log.info("pop:{}", redisUtil.sPop(sKey));
    }

}
