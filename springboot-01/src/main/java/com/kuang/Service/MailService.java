package com.kuang.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class MailService {
    @Autowired
    JavaMailSenderImpl mailSender;

    /**
     * @title senMail
     * @description 邮件设置1：一个简单的邮件
     * @updateTime 2023/10/22 22:45
     * @throws
     */
    public void senMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("通知-明天来狂神这听课");
        message.setText("今晚7:30开会");
        message.setTo("prefule@qq.com");
        message.setFrom("prefule@qq.com");
        mailSender.send(message);
    }

    /**
     * @title sendMail2
     * @description 邮件设置2：一个复杂的邮件
     * @updateTime 2023/10/22 22:43
     * @throws
     */
    public void sendMail2() throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setSubject("通知-明天来狂神这听课");
        helper.setText("<b style='color:red'>今天 7:30来开会</b>",true);

        //发送附件
        helper.addAttachment("1.jpg", new File("1.jpg"));
        helper.addAttachment("2.jpg", new File("1.jpg"));

        helper.setTo("prefule@qq.com");
        helper.setFrom("prefule@qq.com");

        mailSender.send(mimeMessage);
    }
}
