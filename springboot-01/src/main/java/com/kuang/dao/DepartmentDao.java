package com.kuang.dao;

import com.kuang.pojo.Department;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prefule
 * @title
 * @description 部门dao
 * @updateTime 2023/10/20 19:39
 * @throws
 */
@Repository
public class DepartmentDao {

    // 模拟数据库中数据
    public static Map<Integer, Department> departments;

    static {
        // 创建一个部门表
        departments = new HashMap<>();
        departments.put(101, new Department(101, "教学部"));
        departments.put(102, new Department(102, "市场部"));
        departments.put(103, new Department(103, "教研部"));
        departments.put(104, new Department(104, "运营部"));
        departments.put(105, new Department(105, "后勤部"));
    }

    //获取所有部门信息
    public Collection<Department> getDepartments() {
        return departments.values();
    }

    //通过id获取部门
    public Department getDepartmentById(Integer id) {
        return departments.get(id);
    }

}
