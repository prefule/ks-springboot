package com.kuang.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author LiuZhiQiang
 * @title
 * @description Swagger 配置类
 * @updateTime 2023/8/26 23:12
 * @throws
 */
@Slf4j
@Configuration
public class Knife4jConfig {

    @Bean
    public Docket docket1() {
        log.info("准备生成接口文档...");
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("springboot.ks")
                .version("1.0")
                .description("springboot.ks接口文档")
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("springboot.ks接口文档")
                .apiInfo(apiInfo)
                .select()
                //指定生成接口需要扫描的包
                .apis(RequestHandlerSelectors.basePackage("com.kuang"))
                .paths(PathSelectors.any())
                .build();
    }
}