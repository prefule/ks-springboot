package com.kuang;

import com.kuang.Service.MailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.mail.MessagingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class Springboot01ApplicationTests {

    //DI注入数据源
    @Autowired
    DataSource dataSource;

    @Test
    public void opDb() throws SQLException {
        //看一下默认数据源
        System.out.println(dataSource.getClass());
        //获得连接
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        //关闭连接
        connection.close();
    }

    @Autowired
    MailService mailService;

    @Test
    public void sendMail() throws MessagingException {
        // 引入外部字体
        String path = System.getProperty("user.dir");
        mailService.sendMail2();
    }
}
